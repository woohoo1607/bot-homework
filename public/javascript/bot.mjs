const botElem = document.getElementById("klava-bot");
const commentsElem = document.getElementsByClassName('comments')[0];

export const updateBot = msg => {
  commentsElem.innerHTML = msg;
}
