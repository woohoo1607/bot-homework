import { createElement, addClass, removeClass } from "./helper.mjs";
import {updateBot} from "./bot.mjs";

const username = sessionStorage.getItem("username");
const id = sessionStorage.getItem("id") || false;

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", { query: { username, id } });

const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const leftGameDiv = document.getElementById("left-game");
const rightGameDiv = document.getElementById("right-game");
const createRoomBtn = document.getElementById("create-new-room-btn");
const roomsContainer = document.getElementById("rooms-container");

createRoomBtn.addEventListener('click', ()=> {
  const roomName = prompt('Введите название комнаты');
  socket.emit("CREATE_ROOM", { roomName, username });
})

const updateRooms = rooms => {
  if (rooms.length===0) {
    return roomsContainer.innerHTML = "Нет доступных комнат";
  }
  const allRooms = rooms.map(room => {
    const roomButton = createElement({
      tagName: "div",
      className: "room-item flex-centered no-select",
      attributes: { id: room.id }
    });
    const p = createElement({
      tagName: "p",
      className: "",
      attributes: {}
    });
    const users = createElement({
      tagName: "p",
      className: "",
      attributes: {}
    });
    p.innerHTML = room.name;
    users.innerHTML = 'количество пользователей - ' + room.users.length
    const joinBtn = createElement({
      tagName: "button",
      className: "",
      attributes: {}
    });
    joinBtn.innerHTML = 'Join';
    const onJoinRoom = () => {
      socket.emit("JOIN_ROOM", room.id, username);
    };
    roomButton.append(p);
    roomButton.append(users);
    roomButton.append(joinBtn);
    joinBtn.addEventListener("click", onJoinRoom);
    if (!room.isAvailable) {
      addClass(roomButton, 'display-none');
    } else {
      removeClass(roomButton, 'display-none');
    }
    return roomButton
  });
  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
}


socket.on("SUCCESS_CONNECT", res => {
  if (!res.success) {
    alert(`Користувач з ім\`ям ${username} вже підключений`);
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("id");
    window.location.replace("/login");
  } {
    sessionStorage.setItem("id", res.id);

  }
});

const errors = msg => {
  alert(msg);
};

const joinRoomDone = room => {
  if (document.getElementsByClassName("back-btn").length!==0) {
    createUsersList(room.users)
    return
  }
  let index = room.users.findIndex(u => u.username===username)
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
  const container = document.getElementsByClassName('users-container')[0]
  const roomNameEl = createElement({
    tagName: "h2",
    className: "",
    attributes: {}
  });
  const backBtn = createElement({
    tagName: "button",
    className: "back-btn",
    attributes: {}
  });
  const readyBtn = createElement({
    tagName: "button",
    className: "ready-btn",
    attributes: {}
  });
  backBtn.innerHTML = 'Back To Rooms';
  backBtn.addEventListener('click', () => {
    socket.emit("LEAVE_ROOMS", room.id, username);
    removeClass(roomsPage, 'display-none');
    addClass(gamePage, 'display-none');
    leftGameDiv.innerHTML = '';
    container.innerHTML = '';
    leftGameDiv.append(container);
    rightGameDiv.innerHTML = '';
  });
  readyBtn.innerHTML = !room.users[index].isReady ? 'READY' : 'NOT READY';
  readyBtn.addEventListener('click', () => {
    room.users[index].isReady = !room.users[index].isReady;
    socket.emit("CHANGE_READY", room.id, room.users[index]);
  });
  roomNameEl.innerHTML = room.name;
  leftGameDiv.append(roomNameEl);
  leftGameDiv.append(backBtn);
  rightGameDiv.append(readyBtn);
  createUsersList(room.users)
}

const updateRoom = room => {
  let index = room.users.findIndex(u => u.username===username);
  const readyBtn = document.getElementsByClassName("ready-btn")[0];
  readyBtn.innerHTML = !room.users[index].isReady ? 'READY' : 'NOT READY';
  createUsersList(room.users);

}

const startTimer = async (timer,textId) => {
  const backBtn =  document.getElementsByClassName("back-btn")[0];
  const readyBtn = document.getElementsByClassName("ready-btn")[0];
  addClass(backBtn, 'display-none');
  addClass(readyBtn, 'display-none');
  let response = await fetch(`http://localhost:3002/game/texts/${textId}`);
  let text = await response.json();
  const time = createElement({
    tagName: "p",
    className: "time",
    attributes: {}
  });
  time.innerHTML = timer;
  rightGameDiv.append(time);

  const gameZone = createElement({
    tagName: "div",
    className: "display-none game-zone",
    attributes: {}
  });

  const gameTimer = createElement({
    tagName: "p",
    className: "game-timer",
    attributes: {}
  });

  const gameText = createElement({
    tagName: "div",
    className: "game-text-container",
    attributes: {}
  });
  for (let i=0; i<text.data.length; i++) {
    const span = createElement({
      tagName: "p",
      className: "span",
      attributes: {}
    });
    if (text.data[i]==" ") {
      span.innerHTML = '&#8194;'
    } else {
      span.innerHTML = text.data[i];
    }
    gameText.append(span);
  }
  gameTimer.innerHTML = ' seconds left';
  gameZone.append(gameTimer);
  gameZone.append(gameText);
  rightGameDiv.append(gameZone);
}

const updateTimer = (timer, roomId) => {
  const time = document.getElementsByClassName("time")[0];
  const gameZone = document.getElementsByClassName("game-zone")[0];
  if (timer==0) {
    time.remove();
    removeClass(gameZone, 'display-none');
    startGame(roomId);
  }
  time.innerHTML = timer;
}

const startGameTimer = () => {

}

const updateGameTimer = timer => {
  const time = document.getElementsByClassName("game-timer")[0];
  if (timer==0) {
  }
  time.innerHTML = `${timer} seconds left`;
}

const endGame = (res, roomId) => {
  alert(res);
  socket.emit("CHANGE_PROGRESS", roomId, username, '0%');
  socket.emit("CHANGE_READY", roomId, {username: username, progressBar: '0%', isReady: false});
  const backBtn =  document.getElementsByClassName("back-btn")[0];
  const readyBtn = document.getElementsByClassName("ready-btn")[0];
  const gameZone = document.getElementsByClassName("game-zone")[0];
  document.removeEventListener("keypress", ()=>console.log('remove'));
  removeClass(backBtn, 'display-none');
  removeClass(readyBtn, 'display-none');
  gameZone.remove();
};

socket.on("UPDATE_ROOMS", updateRooms);
socket.on("ERRORS", errors);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("UPDATE_ROOM", updateRoom);
socket.on("START_TIMER", startTimer);
socket.on("UPDATE_TIMER", updateTimer);
socket.on("START_GAME_TIMER", startGameTimer);
socket.on("UPDATE_GAME_TIMER", updateGameTimer);
socket.on("END_GAME", endGame);
socket.on("UPDATE_BOT", updateBot);

const createUsersList = users => {
  const container = document.getElementsByClassName('users-container')[0];
  container.innerHTML = "";
  users.map(user=> {
    const userContainer = createElement({
      tagName: "div",
      className: "",
      attributes: {}
    });
    const span = createElement({
      tagName: "span",
      className: user.isReady ? "green" : "red",
      attributes: {}
    });
    const name = createElement({
      tagName: "h4",
      className: "",
      attributes: {}
    });
    const progress = createElement({
      tagName: "div",
      className: "progress",
      attributes: {}
    });
    const userProgress = createElement({
      tagName: "div",
      className: `user-progress ${user.username}`,
      attributes: {}
    });
    userProgress.style.width = user.progressBar;
    user.progressBar==='100%' ?
        userProgress.style.backgroundColor='greenyellow' :
        userProgress.style.backgroundColor='green'
    progress.append(userProgress);
    userContainer.append(span);
    if (user.username===username) {
      name.innerHTML = `${user.username} (you)`;
    } else {
      name.innerHTML = user.username;
    }
    userContainer.append(name);
    userContainer.append(progress);
    container.append(userContainer)
  })
  leftGameDiv.append(container)
};

const startGame = (roomId) => {
  socket.emit("START_GAME", roomId, username);
  const gameTxtContainer = document.getElementsByClassName('game-text-container')[0];
  const myProgress = document.getElementsByClassName(`user-progress ${username}`)[0];
  const children = gameTxtContainer.children;
  const length = gameTxtContainer.children.length;
  let successSymbols = 0;
  children[0].classList.add("next");
  document.addEventListener("keypress", (e) => {

    if (e.key==children[successSymbols].textContent) {
      children[successSymbols].classList.remove("next");
      children[successSymbols].classList.add("finished");
      successSymbols++;
      let progress = (successSymbols/length)*100+"%";
      myProgress.style.width = progress;
      socket.emit("CHANGE_PROGRESS", roomId, username, progress);
      if (successSymbols !== length) {
        children[successSymbols].classList.add("next");
      }
    }
    if (e.key==' ' && children[successSymbols].textContent == ' ') {
      children[successSymbols].classList.remove("next");
      children[successSymbols].classList.add("finished");
      successSymbols++;
      let progress = (successSymbols/length)*100+"%";
      myProgress.style.width = progress;
      socket.emit("CHANGE_PROGRESS", roomId, username, progress);
      if (successSymbols !== length) {
        children[successSymbols].classList.add("next");
      }
    }
  })
}
