export const randomNumber = (maxNumber) => {
  return Math.floor(Math.random() * maxNumber);
}

export const afterSecond = (referenceSec) => {
  return (currentSec) => (currentSec % referenceSec === 0)
}

export const sortByPosition = (users) => {
  return users.sort((a,b) => b.progressBar.slice(0,-1) > a.progressBar.slice(0,-1) ? 1 : -1)
}

export const getSuccessSymbols = (progress, textLength) => {
  return Math.round(((+progress)/100)*textLength)
}
