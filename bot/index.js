import {afterSecond, getSuccessSymbols, randomNumber, sortByPosition} from "../helpers/helpers";
import {texts} from "../data";

//класс Bot реализует абстрактую фабрику. В зависимости от входного значения он будет возвращать разные типы ботов
//при этом все боты должны иметь одинаковый интерфейс (подключить TypeScript и описать какие методы и свойства содержит бот)
class Bot {
  create(type) {
    if (type==='klava-bot') {
      return new KlavaBot()
    }
  }
}

export const createBot = (type) => {
  return new Bot().create(type)
}

class KlavaBot {
  constructor () {
    this.name = 'Джо'
    this.game = 'Клавогонки'
    this.gameTime = 0
    this.gameTextLength = 0
    this.after30Seconds = afterSecond(30)
    this.firstGameAction = true
    this.users = []
    this.finished = []
  }
  sendMessage (msg) {

  }
  hi (username) {
    return `Добро пожаловать в ${this.game}, ${username}! Меня зовут ${this.name} и я буду следить что-бы все было честно.`
  }
  changeUserStatus (username, status) {
    return `${username} поменял свой статус на ${status}`
  }
  // в этом методе используется паттерн фасад
  // мы не заставляем пользователя формировать самому автомобиль наш метод сам создает транспортное средство
  // с определенным цветом (возможно для этого паттерна нужно было прям здесь описать new Color и на основе этого создать
  // транспорт но я не уверен=) )
  getAuto() {
    return new Transport().get()
  }
  beforeStart (users, textId, sendMessageByBot) {
    this.gameTime = 0
    this.sendMessage = sendMessageByBot
    this.gameTextLength = texts[textId].length
    const aboutUsers = users.map(user => user.username + ' который сел в ' + user.auto).join(', ')
    const text = 'Все готовы, поэтому самое время познакомится. В нашем сегодняшнем заезде участвуют: '
    return text + aboutUsers
  }
  updateGameTime (timeLeft) {
    this.gameTime++
    if (this.after30Seconds(this.gameTime) && timeLeft!==0) {
      const aboutUsers = sortByPosition([...this.users]).map((user, i) => {
        return `${i+1} - ${user.username}`
      })
      const msg = `После ${this.gameTime} секунд гонки у нас следующая ситуация: ${aboutUsers.join(', ')}. Страсти наколяются, а до окончания гонки осталось ${timeLeft} секунд`
      this.sendMessage(msg)
    }
    if (timeLeft===0) {
      this.gameEnd()
    }
  }
  gameProgress (users, whoCaused) {
    this.users = users
    if (this.firstGameAction) {
      this.firstGameAction = false
      this.sendMessage(`Первым стартовал ${whoCaused.username}. Его ${whoCaused.auto} со свистом колес вырывается вперед, пока остальные включают первую передачу`)
    }
    this.controlSymbolCount(whoCaused)
  }
  controlSymbolCount (user) {
    const successSymbols = getSuccessSymbols(user.progressBar.slice(0,-1), this.gameTextLength)
    if (this.gameTextLength-successSymbols===30) {
      this.sendMessage(`Что бы добраться до финиша, гонщику ${user.username} осталось преодолеть 30 кнопкометров. Будем надеятся что его ${user.auto} не подведет его`)
    }
    if (this.gameTextLength-successSymbols===7) {
      this.sendMessage(`${user.auto} выезжает на финишную прямую, а за рулем сидит уставший ${user.username}`)
    }
    if (this.gameTextLength === successSymbols) {
      const mood = new DetermineTheMood().getMood(this.finished.length)

      this.finished.push({...user, mood, time: this.gameTime})
      this.sendMessage(`${user.auto} проехал финишную черту, а ${user.username} - ${mood}`)
    }
  }
  gameEnd(isAllFinished = false) {
    if (!isAllFinished) {
      const noFinishedUsers = this.users.filter(user => user.progressBar!=='100%').map(user => {
        user.time = this.gameTime
        return user
      })
      const users = sortByPosition([...noFinishedUsers])
      this.finished.push(...users)
    }
    if (this.finished.length>3) {
      this.finished.splice(3)
    }
    const result = this.finished.map((user, i) => {
      return `${i+1} место - ${user.username} c результатом ${user.time} секунд`
    })
    this.sendMessage(`Всем спасибо за внимание, а теперь подведем итоги: ${result.join(', ')}.`)
  }

}

class Transport {
  constructor() {
    this.cars = ['mercedes', 'bmw', 'жигуль',
      'велосипед', 'renault', 'fiat', 'peugeot',
      'citroen', 'уаз', 'dacia', 'kia', 'skoda',
      'volkswagen', 'porsche', 'ferrari', 'бетмобиль'];
    this.state = ['новый', 'старый', 'поломаный', 'ржавый', 'тюнингованый']
  }
  get () {
    const color = new Color().get();
    return this.state[randomNumber(this.state.length)] + ' ' + color + ' ' + this.cars[randomNumber(this.cars.length)]
  }
}

class Color {
  constructor() {
    this.colors = ['желтый', 'красный', 'фиолетовый',
      'зеленый', 'белый', 'черный',
      'розовый', 'голубой', 'синий',
      'серый', 'коричневый', 'оранжевый',
      'бежевый', 'пурпурный', 'разноцветный']
  }
  get () {
    return this.colors[randomNumber(this.colors.length)]
  }
}
//здесь реализован паттерн фабрика в зависимсоти он того, какое место занял человек ему формируется его настроение
//при этом классы настроения имеют одинаковый интерфейс
class DetermineTheMood {
  getMood(position) {
    if (position === 0) {
      return new NiceMood().get()
    } else if (position < 3) {
      return new GoodMood().get()
    } else {
      return new BadMood().get()
    }
  }
}


class NiceMood {
  constructor() {
    this.moods = ['сияет от счастья', 'находится на 7 небе от счастья', 'не может поверить в происходящее', 'прыгает от радости',
      'безумно довольный', 'довольный как слон', 'благодорит свой скоростной интернет', 'целует клавиатуру', 'падает со стула от радости']
  }
  get() {
    return this.moods[randomNumber(this.moods.length)]
  }
}

class GoodMood{
  constructor() {
    this.moods = ['улыбается', 'делает вид что довольный', 'радуется результату', 'хочет реванша',
      'похрустывает пальцы', 'слегка постукивает по клавиатуре']
  }
  get() {
    return this.moods[randomNumber(this.moods.length)]
  }
}

class BadMood{
  constructor() {
    this.moods = ['ломает клавиатуру', 'жалуется провайдеру на интернет', 'пинает системный блок ногой', 'разбивает монитор',
      'плачет', 'жалуется на меня в тех. поддержку', 'кричит что его засудили']
  }
  get() {
    return this.moods[randomNumber(this.moods.length)]
  }
}
